$(document).ready(function() {

  	var spielerAnzahl = 0; //in der Variable 'spielerAnzahl' wird gespeichert wie viele Spieler es gibt
	var spieler = 1; //Variable, die bestimmt welcher Spieler gerade dran ist
	var spielerArray; //Array, in der alle Informationen zu den Spielern gespeichert werden >> Spieler werden hier gespeichert
	var schonGewuerfelt = false; //Variable, die enthält, ob der Spieler schon gewürfelt hat

	var ereigniskarten; //Array, in dem alle Ereigniskarten gespeichert werden und ihre Informationen 
	var karten; //Array, in dem alle Spielkarten (die Karten, die man kaufen kann) speichert und ihre Informationen 

	//die Höhe des Spielfeldes ist gleich die Breite des Spielfeldes
  	var cw = $('#canvas').width();
  	$('#canvas').css({'height':cw+'px'});

  	//Konsruktor für Spieler
  	function Spieler (id, name, geld, spielfeldId, aktiv) { 
	    this.id = id;
	    this.name = name;
	    this.geld = geld;
	    this.spielfeldId = spielfeldId;
	    this.aktiv = aktiv; //Variable, die speichert, ob der Spieler noch 'im Spiel' ist 
  	}

  	//Konstruktor für die Ereigniskarte
	function Ereigniskarte (id, text, wert) {
		this.id = id;
	    this.text = text;
	    this.wert = wert;
	}

	// Ereigniskarten werden erstellt
	var ereigniskarte1 = new Ereigniskarte('1', 'Du hast in einem Gewinnspiel gewonnen. Ziehe 200 Bitcoins ein.', '200');
	var ereigniskarte2 = new Ereigniskarte('2', 'Die Bank zahlt dir 100 Bitcoins.', '100'); 
	var ereigniskarte3 = new Ereigniskarte('3', 'Dein Handy ist runtergefallen. Zahle 200 Bitcoins für ein Neues.', '-200');
	var ereigniskarte4 = new Ereigniskarte('4', 'Dein Handyguthaben ist leer. Lade dein Handy für 50 Bitcoins auf.', '-50');
	var ereigniskarte5 = new Ereigniskarte('5', 'Rücke bis auf Los vor.', '0');
	var ereigniskarte6 = new Ereigniskarte('6', 'DU wurdest betrunken auf dem Fahrrad erwischt. Zahle 100 Bitcoins Strafe.', '-100');
	var ereigniskarte7 = new Ereigniskarte('7', 'Du erbst 300 Bitcoins.', '300');
	var ereigniskarte8 = new Ereigniskarte('8', 'Zahle deine Flatrate. 50 Bitcoins.', '-50');
	var ereigniskarte9 = new Ereigniskarte('9', 'Du bekommst Taschengeld. 100 Bitcoins.', '100');
	var ereigniskarte10 = new Ereigniskarte('10', 'Du verlierst 50 Bitcoins.', '-50');

	//alle Ereigniskarten werden im Array ereigniskarten gespeichert, damit man später auf sie zugreifen kann
	ereigniskarten=[ereigniskarte1, ereigniskarte2, ereigniskarte3, ereigniskarte4, ereigniskarte5, ereigniskarte6, ereigniskarte7, ereigniskarte8, ereigniskarte9, ereigniskarte10];

	//Konstruktor für Karte
  	function Karte (id, name, kaufwert, hauser, hauswert, miete, spieler) {
	    this.id = id;
	    this.name = name;
	    this.kaufwert = kaufwert;
	    this.hauser = hauser;
	    this.hauswert = hauswert;
	    this.miete = miete;
	    this.spieler = spieler; //In dieser variable soll gespeichert werden wem die Karte gehoert/wer sie gekauft hat
  	}

	  //Karten deklarieren und initialisieren
	  var los = new Karte('1', 'Los', '0', '0', '0', '0', '0');
	  var ask = new Karte('2', 'Ask', '60', '0', '10', '5', '0');
	  var bing = new Karte('3', 'Bing', '80', '0', '20', '7', '0');
	  var ereignis1 = new Karte('4', 'Ereignisfeld', '1000', '0', '0', '0', '0');
	  var yahoo = new Karte('5', 'Yahoo', '100', '0', '30', '10', '0');
	  var google = new Karte('6', 'Google', '120', '0', '40', '12', '0');
	  var gefaengnis = new Karte('7', 'Gefängnis', '0', '0', '0', '0', '0');
	  var pinterest = new Karte('8', 'Pinterest', '140', '0', '15', '500', '0');
	  var tumblr = new Karte('9', 'Tumblr', '160', '0', '60', '17', '0');
	  var ereignis2 = new Karte('10', 'Ereignisfeld', '0', '0', '0', '0', '0');
	  var skype = new Karte('11', 'Skype', '180', '0', '70', '20', '0');
	  var whatsapp = new Karte('12', 'Whatsapp', '200', '0', '80', '22', '0');
	  var freiParken = new Karte('13', 'Frei Parken', '0', '0', '0', '0', '0');
	  var spotify = new Karte('14', 'Spotify', '220', '0', '90', '25', '0');
	  var twitch = new Karte('15', 'Twitch', '240', '0', '100', '27', '0');
	  var ereignis3 = new Karte('16', 'Ereignisfeld', '0', '0', '0', '0', '0');
	  var netflix = new Karte('17', 'Netflix', '260', '0', '110', '30', '0');
	  var youtube = new Karte('18', 'Youtube', '280', '0', '120', '32', '0');
	  var insGefaengnis = new Karte('19', 'Ins Gefängnis', '0', '0', '0', '0', '0');
	  var snapchat = new Karte('20', 'Snapchat', '300', '0', '130', '35', '0');
	  var instagram = new Karte('21', 'Instagram', '320', '0', '140', '37', '0');
	  var ereignis4 = new Karte('22', 'Ereignisfeld', '0', '0', '0', '0', '0');
	  var twitter = new Karte('23', 'Twitter', '350', '0', '150', '45', '0');
	  var facebook = new Karte('24', 'Facebook', '400', '0', '160', '55', '0');

	  //alle Karten werden im Array karten gespeichert, damit man später auf sie zugreifen kann
	  karten = [los, ask, bing, ereignis1, yahoo, google, gefaengnis, pinterest, tumblr, ereignis2, skype, whatsapp, freiParken, spotify, twitch, ereignis3, netflix, youtube, insGefaengnis, snapchat, instagram, ereignis4, twitter, facebook];

	//Infos zu den Karten im PopUp-Fenster
	for(var i = 1; i<=24; i++) {
		$("#info" +karten[i-1].name).append("<img class='ui avatar image' src='images/"+karten[i-1].name +".png' height='50px'>");
  		$("#info" +karten[i-1].name).append("<p id='kaufpreis" +karten[i-1].name +"'>Kaufwert: "+ karten[i-1].kaufwert+" Bitcoins</p><p id='miete" +karten[i-1].name +"'>Miete: "+ karten[i-1].miete+" Bitcoins</p><p id='haeuser"+karten[i-1].name +"'>Häuser: -</p><p id='hauswert"+karten[i-1].name +"'>Kosten pro Haus: "+ karten[i-1].hauswert+" Bitcoins</p><p id='spieler"+karten[i-1].name +"'>Eigentümer: -</p>");
  		 
	}

	//click-Funktion-Zuweisung der Buttons
	$("button#spielzugBeenden").click(function() {
		spielzugBeenden();
	});

	$(".kaufen").click(function() { 
		var ausgesuchteSpielkarte = document.getElementById(this.getAttribute('id')).parentElement.getAttribute('id'); //Die Spielkarte, die gekauft werden soll
		kaufen(ausgesuchteSpielkarte);
	});
	
	//beim klick auf den Button "wuerfeln" wird gewurfelt/Wuerfelfunktion wird aufgerufen
	$("button#wuerfel").click(function() {
		//erst wird ueberprueft, ob der Spieler schon gewuerfelt hat
		if (schonGewuerfelt == false) {
			var wuerfelwert = wuerfeln();
			figurBewegen(wuerfelwert);
			schonGewuerfelt = true;
		}
		else {
			alert("Sie haben schon gewürfelt :)")
		}
	});

	//mit dem Button startButton wird das Spiel gestartet 
	$("button#startButton").click(function() {
		spielStarten();
	});

	//Funktion um Bilder auszutauschen 
	function swap(Bildname,BildURL) {
		document.images[Bildname].src = BildURL;
	}
	
	//Funktion, die aufgerufen wird, wenn das Spiel beendent werden soll
	function spielBeenden() {

		//Abfrage nach einer BEstätigung, ob das Spiel wirklich beendet werden soll
		var bestaetigung = window.confirm('Möchten Sie das Spiel wirklich beenden?');

		//Nach Betätigung des Buttons
		if(bestaetigung) { //OK wurde gedrückt
			//Die Anzeige der Infos zu den Spielern wieder entfernen
			for(var i = 1; i<=spielerAnzahl; i++) {
				$('#spieler' +i).remove();
			}

			//Werte der Karten wieder auf die Ursprungswerte setzen und anzeigen (in den PopUp-Fenstern)
			karten = [los, ask, bing, ereignis1, yahoo, google, gefaengnis, pinterest,tumblr,ereignis2, skype, whatsapp, freiParken, spotify, twitch, ereignis3, netflix, youtube, insGefaengnis, snapchat, instagram, ereignis4, twitter, facebook];

			//Infos zu den Karten im PopUp-Fenster
			for(var i = 1; i<=24; i++) {
		  		$("#kaufpreis" +karten[i-1].name).text("Kaufwert: "+ karten[i-1].kaufwert+" Bitcoins");
		  		$("#miete" +karten[i-1].name).text("Miete: "+ karten[i-1].miete+" Bitcoins");
		  		$("#haeuser" +karten[i-1].name).text("Häuser: -");
		  		$("#hauswert" +karten[i-1].name).text("Kosten pro Haus: "+ karten[i-1].hauswert+" Bitcoins");
		  		$("#spieler" +karten[i-1].name).text("Eigentümer: -");
		  	}

		  	//die alten Spielfiguren von Spielfeld entfernen
		  	for(var i = 1; i <= spielerAnzahl; i++) {
		  		$("#figurSpieler" +i).remove();
		  	}

	  		//Startbutton hinzufuegen
			startButtonErstellen();

			//alle Werte wieder auf die Ursprungswerte setzen
		 	spielerArray = 0;
		 	spielerAnzahl = 0;
		 	spieler = 1;
		 	schonGewuerfelt = false;

		 	//der spiel-Bereich wird ausgeblendet (der Würfel, der würfeln-Button und der spielzugbeenden-Button)
		 	$('#spiel').css({
				display: 'none'
			});

		 	//Das Bild des Würfels wird wieder auf das Standardbild zurückgesetzt
		 	wuerfelStandartSetzen();
		  	
		}
		else {
		  //Abbrechen wurde gedrückt
		}

	}

	//Funktion, die prueft, ob und welcher Spieler gewonnen hat
	function gewinnPruefen() {
		var spielerImSpiel = 0; //Anzahl der Spieler, die im Spiel sind
		var spielerId; //Id des Spielers, der noch im Spiel ist
		
		for(var i = 1; i<= spielerAnzahl; i++) {
			//alle Spieler werden einmal durchgeprüft, ob der Spieler noch mitspielt
			if(spielerArray[i-1].aktiv){
				spielerImSpiel++; //wenn ein Spieler aktiv ist (im Spiel ist) wird spielerImSpiel hochgezählt >> weil es einen Spieler mehr gibt, der mitspielt
				spielerId = spielerArray[i-1].id; //die Id dieses Spielers wird in spielerId gespeichert;
			}
		}

		//wenn nur noch ein Spieler im Spiel ist
		if(spielerImSpiel==1) {
			alert(spielerArray[spielerId-1].name +" hat gewonnen!"); //Ausgabe, dass dieser Spieler gewonnen hat
			spielBeenden(); //Spiel wird beendet
		}

		//wenn kein Spieler mehr im Spiel ist (dies wird benötigt, wenn es von Anfang an nur einen Spieler gab)
		if(spielerImSpiel == 0) {
			spielBeenden(); //Spiel wird beendet
		}
	
	} 
	
	//die Funktion sorgt dafür, dass der nächste Spieler an der Reihe ist
	function naechsterSpieler() {
		do {
			if (spieler < spielerAnzahl) {
				spieler++;
				
			} 
			else {
				spieler = 1; //wenn es z.B. 3 Spieler gibt und Spieler 3 gerade dran war, ist als Nächstes wieder Spieler 1 dran
			};

		}
		while(spielerArray[spieler-1].aktiv == false);	//spieler wird solange hochgezählt bis ein anktiver Spieler an der reihe ist (die Spieler, die bereits verloren haben werden dsadurch übersprungen)
	}

	function spielzugBeenden() {
		if(schonGewuerfelt){ //erst wenn der Spieler gewürfelt hat kann er den Spielzug beenden
			naechsterSpieler(); 
			wuerfelStandartSetzen(); //das Bild des Würfels wird wieder auf den Standardwert gesetzt
			schonGewuerfelt = false; //da der nächste Spieler noch nicht gewürfelt hat wird schonGewuerfelt wieder auf false gesetzt
			$("#konsole").text(spielerArray[spieler-1].name +" ist dran"); //In der "Konsole"(div) steht, welcher Spieler dran ist
		}
		else {
			alert("Sie müssen erst würfeln ;)"); //wenn der Spieler noch nicht gewürfelt hat kommt ein Alert, der ihn darauf hinweist
		}
	}

	function wuerfelStandartSetzen() {
		//Die Bilder der Wuerfel werden wieder durch den Standardwuerfel ersetzt
		javascript:swap("wuerfelBild",'images/wuerfel.jpg');
		
	}

	//Wuerfelfunktion 
	function wuerfeln() {
		var wuerfelwert; //speichert, welche Zahl gewürfelt wurde
		wuerfelwert = Math.floor((Math.random() * 6) + 1); //Wuerfel wuerfeln

		//Wurfel anzeigen mit Augenzahl
		//je nach dem das richtige Bild zum Tauschen aussuchen und tauschen
		for(var j=1; j<=6; j++) {
			if(wuerfelwert == j) {
				var wurfeltausch = 'images/wuerfel'+j +'.jpg';

				javascript:swap("wuerfelBild",wurfeltausch);
			}
			
		}
		return(wuerfelwert); //die gewürfelte Augenzahl zurückgeben
	}

	function figurBewegen(wuerfelwert) {
		var aktuelleSpielfeldId = spielerArray[spieler-1].spielfeldId; //auf diesem Feld befindet sich der Spieler aktuell
		var neueSpielfeldId = parseInt(aktuelleSpielfeldId) + parseInt(wuerfelwert); //das ist das Feld, auf dem er als nächstes steht

		if(parseInt(neueSpielfeldId)>24) { //24 = die höchste FeldId (letztes Feld)
			neueSpielfeldId = parseInt(wuerfelwert)-(24-parseInt(aktuelleSpielfeldId));

			//Spieler erhaelt Geld, weil er ueber 'Los' kommt
			alert("Sie sind über Los gekommen. Sie erhalten 200 Bitcoins."); 
			spielerArray[spieler-1].geld = 200 + parseInt(spielerArray[spieler-1].geld); //der Spieler erhält Geld, da er über Los gekommen ist
		}
		if(neueSpielfeldId==19) { //wenn der Spieler auf das Feld 'GEhen Sie ins Gefängnis' kommt, muss er ins Gefängnis
			alert("Gehen Sie in das Gefängnis!");
			neueSpielfeldId=7;
		}

		spielerArray[spieler-1].spielfeldId = neueSpielfeldId;
		mieteZahlen(neueSpielfeldId); //Spieler muss Miete zahlen auf dem neuen Feld

		//wenn der Spieler auf ein Ereignisfeld kommt wird eine Ereigniskarte gezogen
		if (neueSpielfeldId == 4 || neueSpielfeldId == 10 || neueSpielfeldId == 16 || neueSpielfeldId == 22) {
			ereigniskarteZiehen();
		}

		//Die Infos zu den Spielern werden aktualisiert
		spielerInfoAktualisieren();
	}

	function spielerNameEingabefelderLoeschen() {
		//bevor die Eingabefelder für die Namen generiert werden, werden die alten (die es möglicherweise gibt) geloescht
		//z.B. wenn man vorher 3 Spieler ausgwaehlt hatte sind da drei Eingabefelder
		//wenn dann auf 2 Spieler reduziert wird sollen da keine 5 Eingabefelder, sondern nur 2 Eingabefelder sein
		for (var i = 1; i <= spielerAnzahl; i++) {
			$('#spielerNameEingabe'+i).remove();
		}
	}

	//mit den Buttons, die die Klasse Anzahl tragen kann die Anzahl der Spieler verändert werden
	$(".anzahl").click(function() {
		//die id des Buttons wird in der Variable 'id' gespeichert, damit spaeter damit gearbeitet werden kann
		var id = this.getAttribute('id');
		spielerNameEingabefelderLoeschen();

		//je nach der id des ausgewaehlten Buttons wird die Spieleranzahl angepasst
		if (id == 'anzahl1') {
			spielerAnzahl = 1;
		}
		else if (id == 'anzahl2') {
			spielerAnzahl = 2;
		}
		else if (id == 'anzahl3') {
			spielerAnzahl = 3;
		}
		else if (id == 'anzahl4') {
			spielerAnzahl = 4;
		}

		//entsprechende Anzahl an Spielern deklarieren >> die Spieler werden in dem Array spielerArray gespeichert 
		spielerArray = Array(spielerAnzahl);
		for (var i = 1; i <= spielerAnzahl; i++) {
			//vllt auch hier bei der Deklaration schon Startgeld und Startfeld mit uebergeben, da es sowieso immer gleich ist am Anfang
			spielerArray[i-1] = new Spieler(i, null, null, null, null); 

			//generiert die Eingabefelder fuer die Namen der Spieler
			$("#spielerNamenEingabe").append("<div class='item' id='spielerNameEingabe" +i +"'></div>");
			$("#spielerNameEingabe" +i).append("<img class='ui avatar image' src='images/figur"+i +".png'>");
			$("#spielerNameEingabe" +i).append("<div class='content' id='spielerNameEingabeContent" +i +"'>");
			$("#spielerNameEingabeContent" +i).append("<a class='ui label' id='spielerNameEingabeLabel"+i +"'>");
			$("#spielerNameEingabeLabel" +i).append("<input id='nameSpieler"+i +"' type='text' placeholder='Spieler "+i +"'>");
		
		}

		//Startbutton wird eingeblendet
		$('#startButton').css({
			display: 'inline'
		});
	});

  
	function spielStarten() {
		var eingabeRichtig = eingabenPruefen();
		if(eingabeRichtig) {
			spielerInfoErstellen();
			beendenButtonErstellen();
			$("#konsole").text(spielerArray[spieler-1].name +" ist dran");
		}
	}

	function eingabenPruefen() {
		var arrayPruefergebnisse = new Array(spielerAnzahl);
		for(var i = 1; i<=spielerAnzahl; i++) {
			var name = document.getElementById('nameSpieler' +i).value;
			if(name == null || name == undefined || name == "" || name == " ") {
				alert("Bitte geben Sie den Namen des Spielers " +i +" an.");
				document.getElementById("spielerNameEingabeContent" +i).classList.add('error');
				arrayPruefergebnisse[i-1]=false;
			}
			else {
				document.getElementById("spielerNameEingabeContent" +i).classList.remove('error');
				arrayPruefergebnisse[i-1]=true;
			}
		}

		for(var i = 0; i < spielerAnzahl; i++) {
			if(arrayPruefergebnisse[i]==false){
				return false;
			}
		}
		return true;
	}

	function spielerInfoErstellen() {
		//den Bereich, in dem die Spielerinfo steht, sichtbar machen
		$('#spiel').css({
			display: 'inline'
		});

		for (var i = 1; i <= spielerAnzahl; i++) {

			//den Spielern einen Namen, Geld (Startgeld) und SpielfeldId (Startfeld ist das erste Feld) zuweisen
			//der Name der einzelnen Spieler wird aus dem jeweiligen Textfeld gelesen
			var name = document.getElementById('nameSpieler' +i).value;
			spielerArray[i-1].name = name;
	  		spielerArray[i-1].geld = '1500';
	  		spielerArray[i-1].spielfeldId = '1';
	  		spielerArray[i-1].aktiv = 'true';

			//Anzeige der Infos der Spieler (Geld, aktuelle SpielfeldId, aktiv?)
  			$("#spielerInfo").append("<div class='item' id='spieler" +i +"'></div>");
  			$("#spieler" +i).append("<img class='ui avatar image' src='images/figur"+i +".png'>");
  			$("#spieler" +i).append("<div class='content' id='infosSpieler" +i +"'>");
          	$("#infosSpieler" +i).append("<div class='header'>"+spielerArray[i-1].name +"</div> <p id ='geldSpieler"+ i+"'> Geld: "+ spielerArray[i-1].geld+" Bitcoins </p><p id ='spielfeldSpieler"+ i+"'>Spielfeld: " +spielerArray[i-1].spielfeldId +"</p><p id ='aktivSpieler"+ i+"'>Aktiv: " +spielerArray[i-1].aktiv +"</p>");
			$("div.1").append("<img src='images/figur" +i +".png' width='35' height='40' id='figurSpieler" +i+"' position='relativ' z-index='"+(i+2)+"'>")
		}
	}

	function spielerInfoAktualisieren() {
		//alte Spielfiguren löschen
		for(var i = 1; i <=spielerAnzahl; i++) {
			$("#figurSpieler" +i).remove();
		}

		for (var i = 1; i<= spielerAnzahl; i++) {
			if(spielerArray[i-1].aktiv){
				$("div." +spielerArray[i-1].spielfeldId).append("<img src='images/figur" +i +".png' width='35' height='40' id='figurSpieler" +i+"' z-index='" +i +"'>");
			}
			$("#geldSpieler" +i).text("Geld: " +spielerArray[i-1].geld +" Bitcoins"); //Geldwert des Spielers
			$("#spielfeldSpieler" +i).text("Spielfeld: " +spielerArray[i-1].spielfeldId); //aktuelle SpielfeldId des Spielers (auf welchem Spielfeld befindet sich der Spieler?)
			$("#aktivSpieler" +i).text("Aktiv: " +spielerArray[i-1].aktiv); //ist der Spieler noch im Spiel?
		}
	}

	function kartenInfoAktualisieren() {
		for(var i = 1; i <= 24; i++) {
	
			var eigentuemer = karten[i-1].spieler;
			if(eigentuemer != "0" && eigentuemer != undefined && eigentuemer != null && eigentuemer != 0) { //wenn es einen Eigentümer gibt, soll dieser angezeigt werden 
				$("#spieler" +karten[i-1].name).text("Eigentümer: " +spielerArray[eigentuemer-1].name);
			}
			else {
				$("#spieler" +karten[i-1].name).text("Eigentümer: -");
			}
			$("#miete" +karten[i-1].name).text("Miete: " +karten[i-1].miete +" Bitcoins"); //Miete soll angezeigt werden 
			var hauser = karten[i-1].hauser;
			if(hauser != "0" && hauser != undefined && hauser != null && hauser != 0) { //wenn es Häuser gibt soll die Anzahl angezeigt werden
				$("#haeuser" +karten[i-1].name).text("Häuser: " +karten[i-1].hauser);
			}	
			else {
				$("#haeuser" +karten[i-1].name).text("Häuser: -");
			}
			
		}
	}

	//Funktion, die aufgerufen wird, wenn ein ein Spieler verloren (kein Geld mehr hat)
	function verloren() {
		alert("Sie haben verloren.");
		//alle Karten, die der Spieler besitzt gehören wieder niemandem und die Häuseranzahl wird zurückgesetzt
		for(var i =1; i <=24; i++) {
			if(karten[i-1].spieler==spieler) { 
				karten[i-1].spieler = "0";
				karten[i-1].hauser = "0";
			}
		}
		spielerArray[spieler-1].aktiv = false; 
		$("#figurSpieler" +spieler).remove();
		gewinnPruefen();
		schonGewuerfelt=true;
		spielzugBeenden();
	}

	function startButtonErstellen() {
		//SpielBeenden Button loeschen
		$('#spielBeenden').remove();

		//Startbutton erstellen
		var buttonSpielStarten = $("<button class='ui button' data-tooltip='Beim Klick startet das Spiel' data-position='bottom right' id='startBtn'>Spiel starten</button>");
		$("#menue").append(buttonSpielStarten);

		//dem Spiel-starten-Button die click-Funktion zuweisen >> popUp-Fenster öffnen
		var Smodal = document.getElementById('StartModal');
		var startBtn = document.getElementById("startBtn");
		startBtn.onclick = function() {
			Smodal.style.display = "block";
		}

	}

	function beendenButtonErstellen() {
		//Startbutton entfernen
		$('#startBtn').remove();

		//beendenButton hinzufügen
		var buttonSpielBeenden = $("<button class='ui button' data-tooltip='Beim Klick beenden Sie das Spiel' data-position='bottom right' id='spielBeenden'>Spiel beenden</button>");
		$("#menue").append(buttonSpielBeenden);

		//Eingabefelder für die Namen der Spieler löschen
		spielerNameEingabefelderLoeschen();

		//Startbutton zum Spielstart ausblenden
		$('#startButton').css({
			display: 'none'
		});

		//Spiel-starten-popUp-Fenster schließen/ausblenden
		var Smodal = document.getElementById('StartModal');
		Smodal.style.display = "none";
	

		//dem Spiel-beenden-Button die click-Funktion zuweisen
		buttonSpielBeenden.on( "click", function() {
			spielBeenden();
		});
	}

	function kaufen(spielkarte) {
		
		if(spielerArray[spieler-1].spielfeldId == spielkarte) {
			
			if(parseInt(spielerArray[spieler-1].geld) >= parseInt(karten[spielkarte-1].kaufwert)) {
				var bestaetigung = window.confirm('Möchten Sie wirklich ' +karten[spielkarte-1].name +' für ' +karten[spielkarte-1].kaufwert +' Bitcoins kaufen?');
				//Nach Betätigung des Buttons
				if(bestaetigung) { //OK wurde gedrückt
					spielerArray[spieler-1].geld = parseInt(spielerArray[spieler-1].geld)-parseInt(karten[spielkarte-1].kaufwert);
					karten[spielkarte-1].spieler = spieler;
					spielerInfoAktualisieren();
					kartenInfoAktualisieren();
					alert("Sie haben " +karten[spielkarte-1].name +" gekauft.");
					$('#kaufen'+karten[spielkarte-1].name).remove();

					var buttonHausKaufen = $("<button class='hausKaufen' id='hausKaufen" +karten[spielkarte-1].name +"'>Haus kaufen </button>");
					$("#" +karten[spielkarte-1].id).append(buttonHausKaufen);

					buttonHausKaufen.on( "click", function() {
						var karte = document.getElementById(this.getAttribute('id')).parentElement.getAttribute('id');
						hausKaufen(karte);
					});

				}
				else {
				  //Abbrechen wurde gedrückt
				}
				
			}
			else {
				alert("Sie haben nicht genug Bitcoins, um diese Karte zu kaufen.");
			}
		
			
		}	
		else {
			alert(karten[spielkarte-1].name +" können Sie nicht kaufen.");
		}
		
	}

	function ereigniskarteZiehen() {
		//Zufallsbereich für die variable
		var min = 1;
		var max = 10;
		var x = Math.floor(Math.random() * (max - min + 1)) + min;

		for(var i = min; i<=max; i++) {
			if(x == i) {
				var text = ereigniskarten[x-1].text;
				var wert = parseInt(ereigniskarten[x-1].wert);
				alert(text);

				if(wert < 0) { //wenn der Spieler Geld abgezogen bekommt 
					if(Math.abs(wert) <= parseInt(spielerArray[spieler-1].geld)) {	//prüfen, ob der Spieler genug Geld besitzt, um den Betrag zu bezahlen 
						spielerArray[spieler-1].geld = parseInt(spielerArray[spieler-1].geld)+wert;
					}
					else { //wenn der Spieler nicht genug Geld hat, hat er verloren 
						verloren();
					}
				}
				else { //wenn der Spieler Geld dazu bekommt
					spielerArray[spieler-1].geld = parseInt(spielerArray[spieler-1].geld)+wert; //Wert wird addiert
				}

				if(x==5) { //wenn der Spieler bis auf los vorrücken soll, wird seine SpielfeldId auf 1 gesetzt (Los)
					spielerArray[spieler-1].spielfeldId = 1;
				}
				
				spielerInfoAktualisieren();
			}
		}
	}
  
	function hausKaufen(karte) {

		if(karten[karte-1].spieler == spieler) {
			if(parseInt(karten[karte-1].hauswert) <=  parseInt(spielerArray[spieler-1].geld)) {
				var bestaetigung = window.confirm('Möchten Sie wirklich für ' +karten[karte-1].hauswert +' Bitcoins ein Haus auf ' +karten[karte-1].name +' bauen ?');
				//Nach Betätigung des Buttons
				if(bestaetigung) { //OK wurde gedrückt
					var preis = parseInt(karten[karte-1].hauswert);
					var miete = parseInt(karten[karte-1].miete) + parseInt(karten[karte-1].hauswert);


					karten[karte-1].hauser++;
					spielerArray[spieler-1].geld = parseInt(spielerArray[spieler-1].geld)- preis;

					alert("Sie haben auf " +karten[karte-1].name +" ein Haus gebaut.");

					$("#geldSpieler" +spieler).text("Geld: " +spielerArray[spieler-1].geld +" Bitcoins");
					kartenInfoAktualisieren();
					spielerInfoAktualisieren();

				}
				else {
				  //Abbrechen wurde gedrückt
				}
				
			}
			else {
				alert("Sie haben nicht genug Bitcoins, um ein Haus auf " +karten[karte-1].name +" zu bauen.")
			}	
		}
		else {
			alert("Sie können auf " +karten[karte-1].name +" kein Haus bauen, da Sie nicht Eigentümer sind.")
		}
	}

	function mieteZahlen(spielfeldId) {
		if(karten[spielfeldId-1].spieler!=spieler && karten[spielfeldId-1].spieler!="0" && karten[spielfeldId-1].spieler!=undefined && karten[spielfeldId-1].spieler!=null) {
			alert("Du musst "+karten[spielfeldId-1].miete +" Bitcoins Miete an " +spielerArray[karten[spielfeldId-1].spieler-1].name +" zahlen.");
			if(spielerArray[spieler-1].geld>=karten[spielfeldId-1].miete){
				spielerArray[spieler-1].geld = parseInt(spielerArray[spieler-1].geld) - parseInt(karten[spielfeldId-1].miete);
				spielerArray[karten[spielfeldId-1].spieler-1].geld = parseInt(spielerArray[karten[spielfeldId-1].spieler-1].geld) + parseInt(karten[spielfeldId-1].miete);
				$("#geldSpieler" +spieler).text("Geld: " +spielerArray[spieler-1].geld +" Bitcoins");
				$("#geldSpieler" +karten[spielfeldId-1].spieler).text("Geld: " +spielerArray[spieler-1].geld +" Bitcoins");
			}
			else {
				verloren();
			}
		}
	}

})
